import socket
import sys

# l'argument du script est le port
PORT = int(sys.argv[1])
print(PORT)

# Création de la socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

server_address = ('localhost', PORT)
print('connexion à %s port %s' % server_address)
# Connexion à l'adresse et au port donné en paramètre
sock.bind(server_address)

while True:
    print('\nen attente d\'un message')
    # Réception des données, décodage en utf-8 et affichage
    data, address = sock.recvfrom(4096)
    data_decoded = data.decode('utf-8')
    print(data_decoded)

    if data_decoded:
        # Demande de message à envoyer, encodage, puis envoie au client
        message_to_send = input('Message à envoyer: ')
        message_by = bytes(message_to_send, 'utf-8')
        sent = sock.sendto(message_by, address)
        print('envoie de %s bytes à %s' % (sent, address))


