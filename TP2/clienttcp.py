import socket
import sys

HOST = '127.0.0.1'
PORT = int(sys.argv[1])

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    print(PORT)
    s.connect((HOST, PORT))
    message_to_send = input('Message à envoyer: ')
    message_by = bytes(message_to_send, 'utf-8')
    s.sendall(message_by)
    data = s.recv(1024)
    data_de = data.decode('utf-8')

print('Received', data_de)
