import socket, sys, os

# Vérification des arguments:
PORT = int(sys.argv[1])
# 1 argument : Cet argument est le port et l'adresse st localhost
if len(sys.argv) == 2:
    addr = '127.0.0.1'
else:
    # Deux arguments : Le premier argument est l'adresse du serveur est le second est le port
    if len(sys.argv) == 3:
        addr = sys.argv[2]
        PORT = sys.argv[3]
    # Sinon nombre d'arguments invalides : ERREUR
    else:
        print(sys.stderr, "Nombre d'arguents invalides")
        exit(-1)

print(PORT)
# Création de la socket
tcpsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsocket.bind((addr, PORT))
tcpsocket.listen(10)

# Accept du client
def accept_client(tcpsocket, addr, i):
    while True:
        print("connecté à l'adresse", addr)
        # Réception des données
        data = tcpsocket.recv(1024)
        # Décodage des données en UTF-8
        data_decode = data.decode('utf-8')
        if not data:
             print("Connexion avec le client" +str(i)+"interrompue")
             break
        print("Client " + str(i) + " a envoyé " + data_decode)
        # Demande de message à envoyer, encodage utf-8 et envoi
        message_to_send = input('Message à envoyer: ')
        message_by = bytes(message_to_send, 'utf-8')
        tcpsocket.sendall(message_by)

# Accept et fork avec les différents clients
def server():
    i=1
    while i<=10:
        conn, addr = tcpsocket.accept()
        child_pid = os.fork()
        if child_pid==0:
            print("Connexion réussie avec le client" + str(i) + str(addr) + "\n")
            accept_client(conn, addr, i)
        else:
            i+=1



server()