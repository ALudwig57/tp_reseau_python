import socket
import sys
import select
import queue


PORT1 = int(sys.argv[1])
PORT2 = int(sys.argv[2])
addr = '127.0.0.1'

#Vérifie les arguments lors du lancement du script :
# - S'il y a 3 arguments, le premier argument correspond à l'adresse, le second au premier port, le troisieme au deuxieme port
# - S'íl y a 2 arguments, le premier argument correspond au premier port, le second au second port, et l'adresse est localhost
# - Sinon erreur
def checkargs(PORT1, PORT2, addr):
    if len(sys.argv) == 4:
        addr = sys.argv[1]
        PORT1 = int(sys.argv[2])
        PORT2 = int(sys.argv[3])
    else:
        if len(sys.argv) != 3:
            print(sys.stderr, "Nombre d'arguents invalides")
            exit(-1)


# Créqtion d'une socket qui écoutera sur le port passé en paramètre
def creersock(port):
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server.setblocking(False)
    server.bind((addr, port))
    return server

# Traite deux sockets clientes passées en paramètres.
def traiterclient(server1, server2):
    inputs = [server1, server2]
    outputs = []
    message_queues = {}

    while inputs:
        readable, writable, exceptional = select.select(
            inputs, outputs, inputs)
        for s in readable:
            if (s is server1) or (s is server2):
                connection, client_address = s.accept()
                connection.setblocking(0)
                inputs.append(connection)
                message_queues[connection] = queue.Queue()
            else:
                data = s.recv(1024)
                if data:
                    message_queues[s].put(data)
                    if s not in outputs:
                        outputs.append(s)
                else:
                    if s in outputs:
                        outputs.remove(s)
                    inputs.remove(s)
                    s.close()
                    del message_queues[s]

        for s in writable:
            try:
                next_msg = message_queues[s].get_nowait()
                print(next_msg.decode('utf-8'))
            except queue.Empty:
                outputs.remove(s)
            else:
                message_to_send = input('Message à envoyer: ')
                message_by = bytes(message_to_send, 'utf-8')
                s.send(message_by)

        for s in exceptional:
            inputs.remove(s)
            if s in outputs:
                outputs.remove(s)
            s.close()
            del message_queues[s]

#Vérification des arguments
checkargs(PORT1, PORT2, addr)

#Affichage des informations de connexion
print("Addr : " + addr)
print("Port1 : " + str(PORT1))
print("Port2 : " + str(PORT2))

#Création de deux sockets
server1 = creersock(PORT1)
server2 = creersock(PORT2)

#écoute des deux sockets
server1.listen(5)
server2.listen(5)

while True:
    traiterclient(server1, server2)


