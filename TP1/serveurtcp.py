import socket
import sys


# Le port correspond au premier argument du script
PORT = int(sys.argv[1])
print(PORT)

while True:
    # Création de la socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as tcpsocket:
        tcpsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        tcpsocket.bind(('127.0.0.1', PORT))
        tcpsocket.listen(5)
        conn, addr = tcpsocket.accept()
        with conn:
            print("connecté à l'adresse", addr)
            # reception des données
            data = conn.recv(1024)
            if not data:
                break
            # Décodage des données reçues
            data_decode = data.decode('utf-8')
            # Affichages des donnés reçues
            print(data_decode)
            # Message à envoyer à écrire puis envoie
            message_to_send = input('Message à envoyer: ')
            message_by = bytes(message_to_send, 'utf-8')
            conn.sendall(message_by)



