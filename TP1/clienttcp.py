import socket
import sys

HOST = '127.0.0.1'  # Adresse IP du serveur sur lequel le client se connecte
PORT = int(sys.argv[1])        # Port pour le serveur

# Création de la socket
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    print(PORT)
    # Connexion de la socket
    s.connect((HOST, PORT))
    # Message à envoyer demandé
    message_to_send = input('Message à envoyer: ')
    # Encodage du message en UTF-8
    message_by = bytes(message_to_send, 'utf-8')
    # Envoi du message
    s.sendall(message_by)
    # Réception des données envoyées par le serveur
    data = s.recv(1024)
    # Données décodées
    data_de = data.decode('utf-8')

# Affichage des données reçues
print('Received', data_de)
